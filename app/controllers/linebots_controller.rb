 require 'csv'
class LinebotsController < ApplicationController

  require 'line/bot'

  protect_from_forgery with: :null_session

  before_action :validate_signature

  def validate_signature
    body = request.body.read
    signature = request.env['HTTP_X_LINE_SIGNATURE']
    unless client.validate_signature(body, signature)
      error 400 do 'Bad Request' end
    end
  end

  def client
    @client ||= Line::Bot::Client.new { |config|
      #config.channel_secret = ENV["LINE_CHANNEL_SECRET"]
      #config.channel_token = ENV["LINE_CHANNEL_TOKEN"]
      config.channel_secret = "595eadd5297ef859290c366b98623b2e"
      config.channel_token = "RShrBzYPkeIPiEPqjRW48Ot1/JDY15z00h8w8u7eXx5DafASgSgBKKMyW7Jemz+5wftEj3fRm8F2nl3MVVMO3HM3oryTMrqGlvyH8GTdGjKif9BreXzL/EH1krHHwSig6RQYJSCVIJaGByaz3EM4TAdB04t89/1O/w1cDnyilFU="
    }
  end

  def speak
    body = request.body.read
    events = client.parse_events_from(body)#parseでrubyの構文に書き換える

    events.each do |event|
      case event
      when Line::Bot::Event::Message
        case event.type
        when Line::Bot::Event::MessageType::Text #Textだけに返信するとき
          case event.message['text']
          when '今日の天気を教えて'
            #ここから天気取得
            uri = URI.parse('http://weather.livedoor.com/forecast/webservice/json/v1?city=130010')
            json = Net::HTTP.get(uri)
            weather = JSON.parse(json)
            #ここまで天気取得

            message = {
              type: 'text',
              text: weather['description']['text']
            }
            client.reply_message(event['replyToken'], message)

          when ''
          else
            year = event.message["text"].match(/\d{4}/)
            title = event.message["text"].match(/\d{4}.+の(.+)/)
            data = CSV.read(Rails.root.join("app", "baseball","#{year[0]} Japan Central League Batting Leaders.csv"))
              case title[1]
              when "首位打者"
                message = {
                  type: 'text',
                  text: data[1][0]
                }
                client.reply_message(event['replyToken'], message)

              when "ホームラン王" && "本塁打王"
                names = data[0][10]
                message = {
                  type: 'text',
                  text: names.to_s
                }
                client.reply_message(event['replyToken'], message)

              end



          # else
          #   message = {
          #     type: 'text',
          #     text: '「今日の天気を教えて」と言ったら教えてあげる'
          #   }
          #   client.reply_message(event['replyToken'], message)
          end
        when Line::Bot::Event::MessageType::Image, Line::Bot::Event::MessageType::Video
          response = client.get_message_content(event.message['id'])
          tf = Tempfile.open("content")
          tf.write(response.body)
        end
      end
    end
  end
end
