require 'nokogiri'
require 'open-uri'
require 'csv'

url = "https://www.baseball-reference.com/register/leader.cgi?type=bat&id=8449fb1b"


def generate_csv(url)

  players = []
  ages = []
  teams = []
  games = []
  pa = []
  ab = []
  runs = []
  hits = []
  h2b = []
  h3b = []
  hr = []
  rbi = []
  sb = []
  cs = [] # 盗塁失敗
  bb = []
  so = [] # 三振
  ba = [] # 打率
  obp = []
  ops = []
  dp = [] # 併殺
  hbp = [] # 被死球
  sh = [] # 犠打
  sf = [] # 犠飛
  ibb = [] # 敬遠

  title = []

  header = %W(name age team game pa ab run hit 2b 3b hr rbi sb cs bb so ba obp  ops  dp hbp sh sf ibb)

  charset = nil

  html = open(url) do |f|
            charset = f.charset
            f.read
          end

  doc = Nokogiri::HTML.parse(html, nil, charset)


  doc.xpath("//h1[@itemprop='name']").css('span').each do |node|
    title << node.inner_text
  end

  title = title.join(" ")

  doc.xpath("//td[@data-stat='player']").each do |node|
    player = node.css('a').inner_text
    players.push(player)
  end


  doc.xpath("//td[@data-stat='age']").each do |node|
    ages.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='team_ID']").each do |node|
    teams << node.css('a').inner_text
  end

  doc.xpath("//td[@data-stat='G']").each do |node|
    games.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='PA']").each do |node|
    pa.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='AB']").each do |node|
    ab.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='R']").each do |node|
    runs.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='H']").each do |node|
    hits.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='2B']").each do |node|
    h2b.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='3B']").each do |node|
    h3b.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='HR']").each do |node|
    hr.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='RBI']").each do |node|
    rbi.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='SB']").each do |node|
    sb.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='CS']").each do |node|
    cs.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='BB']").each do |node|
    bb.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='SO']").each do |node|
    so.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='batting_avg']").each do |node|
    ba.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='onbase_plus_slugging']").each do |node|
    ops.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='GIDP']").each do |node|
    dp.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='HBP']").each do |node|
    hbp.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='SH']").each do |node|
    sh.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='SF']").each do |node|
    sf.push(node.inner_text)
  end

  doc.xpath("//td[@data-stat='IBB']").each do |node|
    ibb.push(node.inner_text)
  end

  CSV.open("#{title}.csv", "w", :headers => header , :write_headers => true) do |csv|
  players.count.times do |i|
    csv << [ players[i],
             ages[i],
             teams[i],
             games[i],
             pa[i],
             ab[i],
             runs[i],
             hits[i],
             h2b[i],
             h3b[i],
             hr[i],
             rbi[i],
             sb[i],
             cs[i],
             bb[i],
             so[i],
             ba[i],
             obp[i],
             ops[i],
             dp[i],
             hbp[i],
             sh[i],
             sf[i],
             ibb[i]
    ]
    end
  end
end
